# Auth JWT
___________________________________

## Functions
- **token_from_claims**
- **claims_from_token**
- **validate_token**

## Structs
```rs

#[derive(Serialize, Deserialize, Debug)]
pub struct User{
    pub id: i32,
    pub username: String
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Claims{
    pub sub: String,
    pub iat: usize,
    pub exp: usize,
    pub user: User,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Token{
    pub token: String,
    pub expiration: usize
}

#[derive(Serialize, Deserialize, Debug)]
pub struct AuthToken{
    pub access_token: Token,
    pub refresh_token: Token
}
```

## Struct Implementations
```rs
impl Token {
    // Constructor 
    pub fn new(token: String, expiration: usize) -> Self;
}

impl AuthToken {
    // Constructor
    pub fn new(access_token: Token, refresh_token: Token) -> Self;

    pub fn new_from_claims(claims: Claims) -> Self;
    pub fn new_from_refresh_token(&self) -> Result<Self, &'static str;    
}
```