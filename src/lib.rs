use chrono::{Utc};
use jsonwebtoken::{encode, decode, Algorithm, Header, EncodingKey, DecodingKey, Validation, get_current_timestamp};
use serde::{Deserialize, Serialize};
use dotenv::dotenv;
use rand::distributions::{Alphanumeric, DistString};

#[derive(Serialize, Deserialize, Debug)]
pub struct User{
    pub id: i32,
    pub username: String
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Claims{
    pub sub: String,
    pub iat: usize,
    pub exp: usize,
    pub user: User,
}


#[derive(Serialize, Deserialize, Debug)]
pub struct Token{
    pub token: String,
    pub expiration: usize
}

#[derive(Serialize, Deserialize, Debug)]
pub struct AuthToken{
    pub access_token: Token,
    pub refresh_token: Token
}

impl Claims{
    pub fn new(sub: String, user: User) -> Self {
        dotenv().ok();
        let access_token_expiration = std::env::var("ACCESS_TOKEN_EXPIRATION")
                        .expect("ACCESS_TOKEN_EXPIRATION must be set").parse::<u32>().unwrap();

        Self {
            sub,
            iat: (Utc::now()).timestamp() as usize,
            exp: (Utc::now() + chrono::TimeDelta::try_seconds(access_token_expiration.into()).unwrap()).timestamp() as usize,
            user,
        }
    }
}

impl Token {
    pub fn new(token: String, expiration: usize) -> Self {
        Self {
            token,
            expiration
        }
    }    
}

impl AuthToken {
    pub fn new(access_token: Token, refresh_token: Token) -> Self {
        Self {
            access_token,
            refresh_token
        }
    }

    pub fn new_from_claims(claims: Claims) -> Self {
        dotenv().ok();

        let refresh_token: String = Alphanumeric.sample_string(&mut rand::thread_rng(), 24);

        let access_token_expiration = std::env::var("ACCESS_TOKEN_EXPIRATION")
                        .expect("ACCESS_TOKEN_EXPIRATION must be set").parse::<u32>().unwrap();
        let refresh_token_expiration = std::env::var("REFRESH_TOKEN_EXPIRATION")
                        .expect("REFRESH_TOKEN_EXPIRATION must be set").parse::<u32>().unwrap();

        let access_token = Token::new(token_from_claims(&claims), (Utc::now() + chrono::TimeDelta::try_seconds(access_token_expiration.into()).unwrap()).timestamp() as usize);

        let refresh_token = Token::new(refresh_token, (Utc::now() + chrono::TimeDelta::try_seconds(refresh_token_expiration.into()).unwrap()).timestamp() as usize);

        Self {
            access_token,
            refresh_token
        }
    }

    pub fn new_from_refresh_token(&self) -> Result<Self, &'static str> {      
        if self.refresh_token.expiration < get_current_timestamp() as usize {
            return Err("Refresh token expired");
        }

        let claims = claims_from_token(self.access_token.token.as_str()).unwrap();
        
        Ok(Self::new_from_claims(
            Claims::new(claims.sub, claims.user)
        ))

    }
    
}

pub fn token_from_claims(claims: &Claims) -> String {
    dotenv().ok();
    let secret = std::env::var("JWT_SECRET").expect("JWT_SECRET must be set");
    encode(&Header::default(), claims, &EncodingKey::from_secret(secret.as_ref())).unwrap()
}

pub fn claims_from_token(token: &str) -> Result<Claims, &'static str> {
    dotenv().ok();
    let secret = std::env::var("JWT_SECRET").expect("JWT_SECRET must be set");

    match decode::<Claims>(token, &DecodingKey::from_secret(secret.as_ref()), &Validation::new(Algorithm::HS256)) {
        Ok(token_data) => {
            Ok(token_data.claims)
        },
        Err(_) => {
            Err("Invalid token")
        }
    }
}

pub fn validate_token(token: &str) -> Result<(), &'static str> {
    dotenv().ok();
    let secret = std::env::var("JWT_SECRET").expect("JWT_SECRET must be set");

    match decode::<Claims>(token, &DecodingKey::from_secret(secret.as_ref()), &Validation::new(Algorithm::HS256)) {
        Ok(token_data) => {
            if token_data.claims.exp < get_current_timestamp() as usize {
                return Err("Token expired");
            }
            Ok(())
        },
        Err(_) => {
            Err("Invalid token")
        }
    }
}



#[cfg(test)]
mod token_tests {
    use super::*;
    
    #[test]
    fn can_generate_token(){
        let user = User{id: 1, username: "test".to_string()};
        let claims = Claims::new("test".to_string(), user);
        let token = token_from_claims(&claims);
        let result = claims_from_token(&token).unwrap();

        assert_eq!(result.user.id, 1);
        assert_eq!(result.user.username, "test");
    }

    #[test]
    fn can_validate_token(){
        let user = User{id: 1, username: "test".to_string()};
        let claims = Claims::new("test".to_string(), user);
        let token = token_from_claims(&claims);

        assert!(claims_from_token(token.as_str()).is_ok());
    }

    #[test]
    fn can_invalidate_expired_token(){
        let user = User{id: 1, username: "test".to_string()};
        let claims = Claims::new("test".to_string(), user);
        let token = token_from_claims(&claims);

        std::thread::sleep(std::time::Duration::from_secs(2));

        assert!(validate_token(token.as_str()).is_err());
    }

    #[test]
    fn can_generate_auth_token(){
        let user = User{id: 1, username: "test".to_string()};
        let claims = Claims::new("test".to_string(), user);
        let auth_token = AuthToken::new_from_claims(claims);
        let result = claims_from_token(auth_token.access_token.token.as_str()).unwrap();

        assert!(result.exp > result.iat);
        assert_eq!(result.user.id, 1);
        assert_eq!(result.user.username, "test");
    }

    #[test]
    fn can_refresh_auth_token(){
        let user = User{id: 1, username: "test".to_string()};
        let claims = Claims::new("test".to_string(), user);
        let auth_token = AuthToken::new_from_claims(claims);
        let original_result = claims_from_token(auth_token.access_token.token.as_str()).unwrap();

        

        std::thread::sleep(std::time::Duration::from_secs(2));

        let refreshed_auth_token = auth_token.new_from_refresh_token().unwrap();
        let refreshed_result = claims_from_token(refreshed_auth_token.access_token.token.as_str()).unwrap();

        println!("{:?}", refreshed_auth_token);	
        println!("{:?}", auth_token);
        assert!((refreshed_auth_token.access_token.expiration as usize) > (auth_token.access_token.expiration as usize));
        assert!((refreshed_auth_token.refresh_token.expiration as usize) > (auth_token.refresh_token.expiration as usize));
        assert!((original_result.exp as usize) < (refreshed_result.exp as usize));
        assert_eq!(original_result.user.id, refreshed_result.user.id);
        assert_eq!(original_result.user.username, refreshed_result.user.username);
    }
}